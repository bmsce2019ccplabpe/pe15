#include<stdio.h>
int
main ()
{
  int a[20], n, i, ch, num, num_pos, num_pos1;
  printf ("Enter the size of the array \n");
  scanf ("%d", &n);
  printf ("Start entering array elements \n");
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  printf
    ("What operation to perform? \n 1. Insertion \n 2. Deletion \n");
  scanf ("%d", &ch);
  switch (ch)
    {
case 1:
      printf ("Enter the element to be inserted \n");
      scanf ("%d", &num);
      printf ("Enter the position of the element to be inserted \n");
      scanf ("%d", &num_pos);
      for (i = n - 1; i >= num_pos; i--)
	a[i + 1] = a[i];
      a[num_pos] = num;
      n++;
      printf ("The new array elements are \n");
      for (i = 0; i < n; i++)
	printf ("%d \t", a[i]);
      printf ("\n");
      break;
 case 2:
      printf ("Enter the position of the element to be deleted \n");
      scanf ("%d", &num_pos1);
      for (i = num_pos1; i < n - 1; i++)
	a[i] = a[i + 1];
      n--;
      printf ("The new array elements are \n");
      for (i = 0; i < n; i++)
	printf ("%d \t", a[i]);
      printf ("\n");
      break;

   default:
      printf ("Error! Please choose a valid operation");
      break;
    }
  return 0;
}