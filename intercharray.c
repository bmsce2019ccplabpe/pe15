#include<stdio.h>
int
main ()
{
  int a[20], n, i;
  printf ("enter size of array\n");
  scanf ("%d", &n);
  for (i = 0; i < n; i++)
    scanf ("%d", &a[i]);
  int small, large, small_pos, large_pos, temp = 0;
  small = a[0];
  large = a[0];
  small_pos = 0;
  large_pos = 0;
  for (i = 0; i < n; i++)
    {
      if (a[i] < small)
	{
	  small = a[i];
	  small_pos = i;
	}
      if (a[i] > large)
	{
	  large = a[i];
	  large_pos = i;
	}
    }
  printf ("smallest number=%d is at pos=%d\n", small, small_pos);
  printf ("largest number = %d is at pos=%d\n", large, large_pos);
  temp = a[large_pos];
  a[large_pos] = a[small_pos];
  a[small_pos] = temp;
  printf ("the array after interchanging is\n");
  for (i = 0; i < n; i++)
    printf ("%d\t", a[i]);
  printf ("\n");
  return 0;
}