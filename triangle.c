#include<stdio.h>
#include<math.h>
float input()
{
	float a;
	printf("Enter length of sides \n");
	scanf("%f", &a);
	return a;
}

float compute(float a, float b, float c)
{
	float s = (a + b + c) / 2;
	float area;
	area = sqrt((s * (s - a) * (s - b) * (s - c)));
	return area;
}

void output(float area)
{
	printf("The area of the triangle is = %f \n", area);
}

int main()
{
	float area, a, b, c;
	a = input();
	b = input();
	c = input();
	area = compute(a, b, c);
	output(area);
	return 0;
}